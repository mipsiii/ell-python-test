{
  description = "Elektroline developer's tracker";

  inputs = {
    pyshv.url = "gitlab:silicon-heaven/pyshv/v0.6.1";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    pyshv,
  }:
    with builtins;
    with flake-utils.lib;
    with nixpkgs.lib; let
      pyproject = trivial.importTOML ./pyproject.toml;

      list2attr = list: attr: attrValues (getAttrs list attr);
      pypi2nix = list: list2attr (map (n: elemAt (match "([^ =<>]*).*" n) 0) list);
      requires = pypi2nix pyproject.project.dependencies;
      requires-test = pypi2nix pyproject.project.optional-dependencies.test;
      requires-docs = pypi2nix pyproject.project.optional-dependencies.docs;

      elldev = {
        buildPythonPackage,
        pytestCheckHook,
        pythonPackages,
        setuptools,
        sphinxHook,
      }:
        buildPythonPackage {
          pname = pyproject.project.name;
          inherit (pyproject.project) version;
          format = "pyproject";
          src = builtins.path {
            path = ./.;
            filter = path: type: ! hasSuffix ".nix" path;
          };
          outputs = ["out" "doc"];
          propagatedBuildInputs = requires pythonPackages;
          nativeBuildInputs = [setuptools sphinxHook] ++ requires-docs pythonPackages;
          nativeCheckInputs = [pytestCheckHook] ++ requires-test pythonPackages;
        };
    in
      {
        overlays = {
          pythonPackagesExtension = final: prev: {
            elldev = final.callPackage elldev {};
          };
          noInherit = final: prev: {
            pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [self.overlays.pythonPackagesExtension];
          };
          default = composeManyExtensions [
            pyshv.overlays.default
            self.overlays.noInherit
          ];
        };
      }
      // eachDefaultSystem (system: let
        pkgs = nixpkgs.legacyPackages.${system}.extend self.overlays.default;
      in {
        packages.default = pkgs.python3Packages.elldev;
        legacyPackages = pkgs;

        devShells = filterPackages system {
          default = pkgs.mkShell {
            packages = with pkgs; [
              editorconfig-checker
              gitlint
              ruff
              (python3.withPackages (p:
                [p.build p.twine p.sphinx-autobuild p.mypy]
                ++ foldl (prev: f: prev ++ f p) [] [
                  requires
                  requires-docs
                  requires-test
                ]))
            ];
          };
        };

        apps.default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/foo";
        };

        checks.default = self.packages.${system}.default;

        formatter = pkgs.alejandra;
      });
}
