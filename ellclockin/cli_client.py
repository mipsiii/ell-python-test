import asyncio
import json
import pathlib
import re

import xdg.BaseDirectory
from prompt_toolkit import PromptSession
from prompt_toolkit.history import FileHistory
from prompt_toolkit.patch_stdout import patch_stdout
from shv import RpcError, RpcLogin, RpcUrl

from venv import logger

from shvcli import builtin_impl as _  # noqa F401
from shvcli.builtin import call_builtin
from shvcli.client import Node, SHVClient
from shvcli.complet import CliCompleter
from shvcli.config import CliConfig
from shvcli.lsdir import dir_method, ls_method
from shvcli.parse import parse_line
from shvcli.scan import scan_nodes
from shvcli.tools import print_cpon
from shvcli.valid import CliValidator

from database import ServerDatabase
from __main__ import parse_args
import argparse

db = ServerDatabase()
args = parse_args()

# async def _app(config: CliConfig, shvclient: SHVClient) -> None:
#     """CLI application."""
#     histfile = pathlib.Path.home() / ".shvcli.history"
#     if not histfile.exists():
#         with histfile.open("w") as _:
#             pass

#     completer = CliCompleter(shvclient, config)
#     validator = CliValidator(shvclient, config)

#     session: PromptSession = PromptSession(
#         history=FileHistory(str(histfile)),
#         completer=completer,
#         validator=validator,
#     )
#     while True:
#         try:
#             prompt_path = (
#                 "ansibrightred"
#                 if shvclient.tree.get_path(config.path) is None
#                 else "ansibrightblue",
#                 config.shvpath(),
#             )
#             try:
#                 with patch_stdout():
#                     result = await session.prompt_async(
#                         [prompt_path, ("", "> ")], vi_mode=config.vimode
#                     )
#             except EOFError:
#                 shvclient.client.disconnect()
#                 return
#             await handle_line(shvclient, config, result)
#         except KeyboardInterrupt:
#             continue


# async def run(config: CliConfig) -> None:
#     """Loop to run interactive CLI session."""
#     shvclient = await SHVClient.connect(config.url)
#     assert isinstance(shvclient, SHVClient)

#     if config.cache:
#         cacheurl = RpcUrl(
#             location=config.url.location,
#             port=config.url.port,
#             protocol=config.url.protocol,
#             login=RpcLogin(username=config.url.login.username),
#         )
#         cpath = xdg.BaseDirectory.save_cache_path("shvcli")
#         fname = re.sub(r"[^\w_. -]", "_", cacheurl.to_url())
#         cachepath = pathlib.Path(cpath).expanduser() / fname
#         if cachepath.exists():
#             with cachepath.open("r") as f:
#                 shvclient.tree = Node.load(json.load(f))
#     if config.initial_scan:
#         await scan_nodes(shvclient, "", config.initial_scan_depth)

#     clitask = asyncio.create_task(_app(config, shvclient))
#     await shvclient.client.wait_disconnect()
#     if not clitask.done():
#         print("Disconnected.")
#         clitask.cancel()
#     try:
#         await clitask
#     except asyncio.CancelledError:
#         pass

#     if config.cache:
#         cachepath.parent.mkdir(exist_ok=True)
#         with cachepath.open("w") as f:
#             json.dump(shvclient.tree.dump(), f)
            
async def handle_line(shvclient: SHVClient, config: CliConfig, cmdline: str) -> None:
    """Handle single command line invocation."""
    items = parse_line(cmdline)
    if items.method == "login":
        # Implement login logic here
        current_project = db.current_project(args.user)
        if current_project:
            logger.info(f"User {args.user} logged in to project {args.project}.")
            logger.info("You must to logout from project if you will other project")
        else:
            if args.project is None:
                logger.error("Project is required for logging in.")
                return
            else:
                logger.info(f"User {args.user} logged in to project {args.project}.")
                db.work(args.user, args.project)
    elif items.method == "logout":
        # Implement logout logic here
        if args.project is None:
            logger.info(f"User {args.user} logged out from all projects.")
        else:
            logger.info(f"User {args.user} logged out from project {args.project}.")
            db.work(args.user, args.project)
    elif items.method == "newworker":
        # Implement newworker logic here
        db.new_worker(args.newworked)
        logger.info(f"User {args.user} are add")
    elif items.method == "newproject":
        # Implement newproject logic here
        db.new_project(args.newproject)
        logger.info(f"User {args.user} are add")
    elif items.method == "users":
        logger.info("List all workers")
        db.workers()
        logger.info()
    else:
        logger.error("Invalid command.")