"""Implementation of command line application."""

import argparse
import logging
from typing import Optional
from database import ServerDatabase

from . import VERSION

logger = logging.getLogger(__name__)


def parse_args() -> argparse.Namespace:
    """Parse passed arguments and return result."""
    parser = argparse.ArgumentParser(prog="ellclockin", description="Foo counter")
    parser.add_argument("--version", action="version", version="%(prog)s " + VERSION)
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        help="Increase verbosity level of logging",
    )
    parser.add_argument(
        "-q",
        action="count",
        default=0,
        help="Decrease verbosity level of logging",
    )
    parser.add_argument(
        "command",
        choices=["login", "logout"],  # Choise, login or logout
        help="Choose command: login to clock in, logout to clock out. Must be user",
    )
    parser.add_argument(
        "--user",
        required=True,  # User name must be mandatory
        help="Username for clocking in or out.",
    )
    parser.add_argument(
        "--users",
        required=True,  # User name must be mandatory
        help="Username for clocking in or out.",
    )
    parser.add_argument(
        "--project",
        required=False,  # project can be optional
        help="Project to clock in or out from.",
    )
    parser.add_argument(
        "--newproject",
        required=True,  # project can be optional
        help="Add new Project. You must also provide the name of the project",
    )
    parser.add_argument(
        "--newworker",
        required=True,  # project can be optional
        help="Add new worker. You must also provide the name of the worker",
    )
    
    return parser.parse_args()
   
def main() -> None:
    """Application's entrypoint."""
    args = parse_args()

    log_levels = sorted(logging.getLevelNamesMapping().values())[1:]
    logging.basicConfig(
        level=log_levels[sorted([1 - args.v + args.q, 0, len(log_levels) - 1])[1]],
        format="[%(asctime)s] [%(levelname)s] - %(message)s",
    )

    # TODO continue with your code
    # Create an instance of ServerDatabase
    db = ServerDatabase()  
    
    if args.command == "login":
        currentProject = db.current_project(args.user)
        if currentProject:
            logger.info(f"User {args.user} logged in to project {args.project}.")
            logger.info("You must to logout from project if you will other project")
        else:
            if args.project is None:
                logger.error("Project is required for logging in.")
                return
            else:
                logger.info(f"User {args.user} logged in to project {args.project}.")
                db.work(args.user, args.project)
            
    elif args.command == "logout":
        # logout from project
        if args.project is None:
            logger.info(f"User {args.user} logged out from all projects.")
        else:
            logger.info(f"User {args.user} logged out from project {args.project}.")
            db.work(args.user, args.project)
    else:
        logger.error("Invalid command.")
    
    if args.newworker:
        db.new_worker(args.newworked)
        logger.info(f"User {args.user} are add")
    
    if args.newproject:
        db.new_project(args.newproject)
        logger.info(f"User {args.user} are add")