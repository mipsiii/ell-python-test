=======================
Elektroline Python test
=======================

This project includes introduction test for Python developers. The point of it
is to check ability to not only code in Python but also to problem solve,
documentation reading and writing, test implementation and project maintenance.
It shoots for the stars so don't be scared.

You should start by reading the documentation! 😉

This project is based on our internal Python project template and thus it
contains a lot of real stuff.

* `📃 Sources <http://gitlab.com/cynerd/ell-python-test>`__
* `📕 Documentation <https://cynerd.gitlab.io/ell-python-test/>`__

Installation
------------

The installation can be done with any of your favorite Python installation tool.
Installation with Pip would be:

.. code-block:: console

   $ git clone https://gitlab.com/Cynerd/ell-python-test.git
   $ pip install ell-python-test

Running tests
-------------

This project contains basic tests in directory tests.

To run tests you have to use **pytest**. To run all tests just run it in the top
level directory of the project. See the `pytest documentation
<https://docs.pytest.org/>`__ for more info.


Documentation
-------------

The documentation is available in `docs` directory. You can build it using:

    sphinx-build -b html docs docs-html
