"""SHV RPC API validation."""

import asyncio

import pytest
import shv

from ellclockin.server.database import ServerDatabase


@pytest.fixture(name="testdb")
def fixture_testdb(server_config):
    db = ServerDatabase(server_config)
    db.new_worker("franta")
    db.new_worker("lojza")
    db.new_project("research")
    db.new_project("test")
    db.new_project("deploy")
    db.add_work("lojza", "research", 10000)
    db.add_work("lojza", "test", 5000)
    db.add_work("lojza", "deploy", 600)
    db.complete_project("research")
    db.work("lojza", "deploy")


@pytest.mark.parametrize(
    "path,expected",
    (
        ("", [".app", ".broker", "clockin"]),
        (".app", []),
        (".broker", ["currentClient", "client"]),
        ("clockin", ["workers", "projects"]),
        ("clockin/workers", ["self", "franta", "lojza"]),
        ("clockin/workers/franta", []),
        ("clockin/workers/lojza", []),
        ("clockin/projects", ["deploy", "research", "test"]),
        ("clockin/projects/deploy", []),
        ("clockin/projects/test", []),
    ),
)
async def test_ls(franta, path, expected, testdb):
    """Check that regular user (franta) can see what we want him to see."""
    assert await franta.ls(path) == expected


@pytest.mark.parametrize(
    "path,expected",
    (
        ("clockin", [shv.RpcMethodDesc.stddir(), shv.RpcMethodDesc.stdls()]),
        ("clockin/workers", [shv.RpcMethodDesc.stddir(), shv.RpcMethodDesc.stdls()]),
        (
            "clockin/workers/self",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "currentProject",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="OptionalString",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "currentSeconds",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="OptionalInt",
                    access=shv.RpcMethodAccess.READ,
                ),
            ],
        ),
        (
            "clockin/workers/franta",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "currentProject",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="OptionalString",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "currentSeconds",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="OptionalInt",
                    access=shv.RpcMethodAccess.READ,
                ),
            ],
        ),
        (
            "clockin/workers/lojza",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "currentProject",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="OptionalString",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "currentSeconds",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="OptionalInt",
                    access=shv.RpcMethodAccess.READ,
                ),
            ],
        ),
        (
            "clockin/projects",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "new", param="String", access=shv.RpcMethodAccess.COMMAND
                ),
                shv.RpcMethodDesc(
                    "clockout",
                    flags=shv.RpcMethodFlags.USER_ID_REQUIRED,
                    access=shv.RpcMethodAccess.WRITE,
                ),
            ],
        ),
        (
            "clockin/projects/research",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "worktime",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="Int",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "workers",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="List[String]",
                    access=shv.RpcMethodAccess.READ,
                ),
            ],
        ),
        (
            "clockin/projects/test",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "worktime",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="Int",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "workers",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="List[String]",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "work",
                    flags=shv.RpcMethodFlags.USER_ID_REQUIRED,
                    access=shv.RpcMethodAccess.WRITE,
                ),
                shv.RpcMethodDesc(
                    "complete",
                    access=shv.RpcMethodAccess.COMMAND,
                ),
            ],
        ),
        (
            "clockin/projects/deploy",
            [
                shv.RpcMethodDesc.stddir(),
                shv.RpcMethodDesc.stdls(),
                shv.RpcMethodDesc(
                    "worktime",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="Int",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "workers",
                    flags=shv.RpcMethodFlags.GETTER,
                    result="List[String]",
                    access=shv.RpcMethodAccess.READ,
                ),
                shv.RpcMethodDesc(
                    "work",
                    flags=shv.RpcMethodFlags.USER_ID_REQUIRED,
                    access=shv.RpcMethodAccess.WRITE,
                ),
                shv.RpcMethodDesc(
                    "complete",
                    access=shv.RpcMethodAccess.COMMAND,
                ),
            ],
        ),
    ),
)
async def test_dir(franta, path, expected, testdb):
    """Check that regular user (franta) can see what we want him to see."""
    assert await franta.dir(path) == expected


async def test_new_project(franta):
    """Create projects interactivelly in clean DB."""
    assert await franta.ls("clockin/projects") == []
    await franta.call("clockin/projects", "new", "test")
    assert await franta.ls("clockin/projects") == ["test"]
    await franta.call("clockin/projects", "new", "deploy")
    assert await franta.ls("clockin/projects") == ["deploy", "test"]


async def test_new_project_invalid(franta):
    with pytest.raises(shv.RpcInvalidParamsError):
        await franta.call("clockin/projects", "new", 3)


async def test_project_state(franta, testdb):
    assert await franta.call("clockin/projects/research", "worktime") == 10000
    assert await franta.call("clockin/projects/research", "workers") == ["lojza"]
    assert await franta.call("clockin/projects/test", "worktime") == 5000
    assert await franta.call("clockin/projects/test", "workers") == ["lojza"]
    assert await franta.call("clockin/projects/deploy", "worktime") == 600
    assert await franta.call("clockin/projects/deploy", "workers") == ["lojza"]


async def test_work(franta, testdb):
    await franta.call("clockin/projects/test", "work")
    await asyncio.sleep(1)
    assert await franta.call("clockin/workers/franta", "currentProject") == "test"
    assert 1 <= await franta.call("clockin/workers/franta", "currentSeconds") < 3
    assert await franta.call("clockin/workers/self", "currentProject") == "test"
    assert 1 <= await franta.call("clockin/workers/self", "currentSeconds") < 3
    await franta.call("clockin/projects", "clockout")
    assert await franta.call("clockin/workers/franta", "currentProject") is None
    assert await franta.call("clockin/workers/franta", "currentSeconds") is None


async def test_complete(franta, testdb):
    assert await franta.dir_exists("clockin/projects/test", "work") is True
    await franta.call("clockin/projects/test", "complete")
    assert await franta.dir_exists("clockin/projects/test", "work") is False


async def test_empty(franta, lojza):
    assert await franta.ls("clockin/projects") == []
    await franta.call("clockin/projects", "new", "test")
    await lojza.call("clockin/projects", "new", "deploy")
    assert await franta.ls("clockin/projects") == ["deploy", "test"]
    assert await franta.ls("clockin/workers") == ["self"]
    await franta.call("clockin/projects/test", "work")
    await lojza.call("clockin/projects/deploy", "work")
    assert await franta.ls("clockin/workers") == ["self", "franta", "lojza"]
    assert await franta.call("clockin/workers/self", "currentProject") == "test"
    assert await lojza.call("clockin/workers/self", "currentProject") == "deploy"
