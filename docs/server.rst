Ellclockin Server
=================

Ellclockin server is all in one server that uses SHV RPC Broker for user
management and Sqlite3 for data retention.

Running
-------

The server can be run with command ``ellclockin-server`` after installation. The
first argument must contain path to the configuration server should use.

.. code-block:: console

   $ ellclockin-server ellclockin-server.ini


Configuration
-------------

Configuration file is in INI format and the following fields are allowed:

:server.port:
  The TCP/IP server port used to listen for connections. The default is
  ``3755``.

:db.file:
  The path to the Sqlite3 database file. New database is create if there is no
  such file. The leading directory won't be created. The default is the current
  work directory and file name ``ellclockin.db``.

:users:
  This section must contain all users of the Ellclockin server. They are
  specified in pairs where key is user's name and value is SHA1 password.


.. literalinclude:: ../tests/server/ellclockin-server.ini
 :language: ini
