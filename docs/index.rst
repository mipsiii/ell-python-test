.. include:: ../README.rst

Introduction
------------

Our company decided that its about time we would have our own implementation of
project clock tracking. The work in our company is split between different
projects and development time consumption must be tracked.

The simple server was implemented and your primary task is to implement CLI
client for developers to use, but to do that you unfortunately will have to
modify server as well.

The communication between server and client is over `Silicon
Heaven <https://silicon-heaven.github.io/shv-doc/>`__ RPC protocol. You can
start the server and investigate its RPC API with `SHVCli
<https://gitlab.com/silicon-heaven/shvcli/>`__:

.. code-block:: console

   $ python3 -m ellclockin.server -vvv tests/server/ellclockin-server.ini

.. code-block:: console

   $ shvcli 'tcp://franta@localhost?shapass=00af70ce92fa1a0106b28f8909a6fe3e7d0ab1d5'
   > ls
   .app .broker clockin
   > clockin/
   clockin> ls
   workers projects
   clockin> ls workers
   self
   clockin> ls projects

   clockin> dir projects
   dir ls new clockout
   clockin> projects:new "test"
   null
   clockin> ls projects
   test
   clockin> projects/test:work
   null
   clockin> ls workers
   self franta
   clockin> workers/self:currentProject
   "test"
   clockin> workers/self:currentSeconds
   18
   clockin> workers/self:currentSeconds
   21
   clockin> projects:clockout
   null
   clockin> workers/self:currentProject
   null
   clockin> workers/self:currentSeconds
   null
   clockin> projects/test:workers
   ["franta"]
   clockin> projects/test:worktime
   31
   clockin>

The concept is that workers start their work by calling ``work`` method on
project they are going to work. Later they can switch to a different project
with the same method (worker can work only on one project at the time). To leave
the work worker must call ``ellclockin/projects:clockout``.

Checklist
---------

This is checklist of expected work you should do. It is not an issue if you are
unable to do them all. If you get stuck on some step then try to workaround it
instead of abandoning the whole ordeal.

- Implement command line client for developers to use to clock-in and out. The
  design is up to you (you would be the user). The file `ellclockin/__main__.py`
  contains initial template for you to start. It must provide a way to start
  working on the project, switch project and clock-out.
- Write user documentation for the tool.
- Add query for the worker's contributions in the project
- Add retroactive clock-out of the workers (if you fail to clock-out).
- Commit changes using Git in the meaningful steps with an appropriate commit
  comments.
- Fork this project on Gitlab.com and submit pull request with your changes.
- Ensure that Gitlab CI passes

Optional points for improving the database queries in
`ellclockin/server/databse.py`.

Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   client
   server
   api/index

:ref:`genindex`
